#!/usr/bin/env bash

set -e

# Get latest changes for aws-cdk packages
RESULT=$(ncu --jsonUpgraded -p yarn "/^@aws-cdk.*$/")

# Count lines (>1 line: json object has changes)
CHANGES_COUNT=$(echo "$RESULT" | wc -l)

if [[ "$CHANGES_COUNT" -eq 1 ]]; then
  echo "Everything is up-tp-date. Nothing to do..."
else
  echo "Update triggered..."
  LATEST_VERSION=$(echo "$RESULT" | jq '[. | to_entries[] | .value ] | first' | sed -e 's/^"//' -e 's/"$//')

  echo "... Run ncu ..."
  ncu -p yarn -u

  echo "... Update will be done to version: $LATEST_VERSION ..."
  yarn version --new-version "$LATEST_VERSION"

  echo "Push to repo..."
  git push https://oauth2:${REPO_ACCESS_TOKEN}@gitlab.com/"$CI_PROJECT_PATH".git HEAD:master --follow-tags

  echo "... done!"
fi
