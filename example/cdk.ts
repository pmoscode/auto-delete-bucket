#!/usr/bin/env node
import {App, Aspects, Tag} from '@aws-cdk/core';
import {ExampleStack} from './stacks/example-stack';

const cdk = new App();
const example = new ExampleStack(cdk, 'auto-bucket-example', {});

Aspects.of(example).add(new Tag('example', 'true'));
