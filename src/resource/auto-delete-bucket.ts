import {Construct, CustomResource, Duration, RemovalPolicy} from '@aws-cdk/core';
import * as path from 'path';
import * as s3 from '@aws-cdk/aws-s3';
import {Code, Runtime, SingletonFunction} from '@aws-cdk/aws-lambda';

const defaultAutoDeleteBucketProps: s3.BucketProps = {
    removalPolicy: RemovalPolicy.DESTROY
};

export class AutoDeleteBucket extends s3.Bucket {
    constructor(scope: Construct, id: string, props: s3.BucketProps) {
        props = {...defaultAutoDeleteBucketProps, ...props};
        super(scope, id, props);

        if (props.removalPolicy == RemovalPolicy.DESTROY) {
            const lambda = new SingletonFunction(this, 'AutoDeleteBucketHandler', {
                uuid: '38ecec1a-ce06-47bc-b8fb-ac83adab617f',
                runtime: Runtime.NODEJS_12_X,
                code: Code.fromAsset(path.join(__dirname, '../lambda')),
                handler: 'main.handler',
                lambdaPurpose: 'AutoDeleteBucket',
                timeout: Duration.minutes(15)
            });

            this.grantReadWrite(lambda);

            new CustomResource(this, 'AutoDeleteBucket', {
                serviceToken: lambda.functionArn,
                resourceType: 'Custom::AutoDeleteBucket',
                properties: {
                    BucketName: this.bucketName
                }
            });
        }
    }
}
